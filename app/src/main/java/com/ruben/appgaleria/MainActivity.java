package com.ruben.appgaleria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button botonS, botonA;
    TextView texto;
    ImageView imagen;
    int[] fotoId = { R.drawable.imagen1, R.drawable.imagen3, R.drawable.imagen4, R.drawable.imagen5,
            R.drawable.imagen6, R.drawable.imagen7};
    String[] txt = { "Soleria", "Soleria", "Piscina", "Cesped artificial", "Madera","Futbol"};
    int cont = 0;
    int total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonS = (Button) (findViewById(R.id.botonS));
        botonA = (Button) (findViewById(R.id.botonA));
        texto = (TextView) (findViewById(R.id.texto));
        imagen = (ImageView) (findViewById(R.id.imagen));
        botonS.setOnClickListener(this);
        botonA.setOnClickListener(this);
        total = fotoId.length;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.botonS) {
           cont++;
            if (cont == total) {
                cont = 0;
            }
        }
        if (id == R.id.botonA) {
            cont--;
            if (cont == -1) {
                cont = total - 1;
            }
        }

        imagen.setImageResource(fotoId[cont]);
        texto.setText(txt[cont]);
    }

    public void llamar(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "626622141"));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void initInformacion(View view) {
        Intent intentInfo=new Intent(this,informacion.class);
        startActivity(intentInfo);
    }
}
